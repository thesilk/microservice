package main

import (
	"gitlab.com/thesilk/microservice/rewards/server"
)

func main() {
	var server server.Server
	server.Initialize()
	server.Run()

}
