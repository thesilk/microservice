package server

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/thesilk/microservice/protobuf"
	"gitlab.com/thesilk/microservice/rewards/model"
	"gitlab.com/thesilk/microservice/rewards/service"
	"google.golang.org/grpc"
)

type Server struct {
	db          *sql.DB
	protoServer *grpc.Server
	err         error
}

func (s *Server) createDBConnection() {
	host := getEnv("DB_HOST", "127.0.0.1")
	port, err := strconv.ParseInt(getEnv("DB_PORT", "3306"), 10, 32)
	if err != nil {
		log.Printf("couldn't convert environment variable DB_PORT to int32: %v\n", err)
	}

	dbUrl := fmt.Sprintf("user:password@tcp(%s:%d)/poc", host, port)

	s.db, s.err = sql.Open("mysql", dbUrl)
	if s.err != nil {
		log.Println(s.err)
	}
}

func (s *Server) createGRPCServer() {
	s.protoServer = grpc.NewServer()
}

func (s *Server) Initialize() {
	s.createDBConnection()
	s.createGRPCServer()
}

func (s *Server) Run() {
	address := "0.0.0.0:1337"
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("Error %v", err)
	}
	fmt.Printf("Server is listening on %v ...\n", address)

	protobuf.RegisterRewardServiceServer(s.protoServer, s)

	s.protoServer.Serve(lis)
}

func (s *Server) Reward(ctx context.Context, request *protobuf.RewardSearchRequest) (*protobuf.RewardSearchResponse, error) {
	lastDays := request.LastDays
	username := request.Username
	var (
		reward int32
		user   *protobuf.User
		err    error
	)
	host := getEnv("SESSION_HOST", "127.0.0.1")
	port, err := strconv.ParseInt(getEnv("SESSION_PORT", "1338"), 10, 32)
	if err != nil {
		log.Printf("couldn't convert environment variable SESSION_PORT to int32: %v\n", err)
	}

	protoUser := service.NewProtoUsers(host, int32(port))
	defer protoUser.Close()
	user, err = protoUser.GetUser(username)
	if err != nil {
		log.Printf("Couldn't find user %s in database\n", username)
	}

	reward, err = model.CalcReward(s.db, user.Id, lastDays)
	if err != nil {
		log.Printf("Coudln't get calculated sum for %s\n", username)
	}

	return &protobuf.RewardSearchResponse{Reward: reward}, nil
}

func getEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = fallback
	}
	return value
}
