package model

import (
	"database/sql"
	"time"
)

func CalcReward(db *sql.DB, id int32, lastDays int32) (int32, error) {
	var reward int32

	lastDaysUnixTime := int32(time.Now().Unix()) - lastDays*3600*24
	err := db.QueryRow("SELECT SUM(reward) FROM rewards WHERE userID=? AND timestamp > ?;", id, lastDaysUnixTime).Scan(&reward)

	return reward, err

}
