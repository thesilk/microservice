# Golang Mircoservice PoC

## Setup

It's very easy to start this setup. You only have to be installed

- `docker`
- `docker-compose`.

It uses a MariaDB with dummy values which you can find in (database/mariadb-dump)[database/mariadb-dump]. These dummy values will be inserted each time you will start this setup for the first time. To start, type:

```bash
$ docker-compose -f docker-compose.yml up
```

## Services

| Service           | Port      | Direct Access from Localhost  |
| ----------------- | --------- | ----------------------------- |
| MariaDB           | 3306      | v                             |
| Adminer           | 8080      | v                             |
| API Gateway       | 2342      | v                             |
| Reward Component  | 1337      | x                             |
| Session Component | 1338      | x                             |



## Credentials Adminer

| Key      | Value    |
| -------- | -------- |
| System   | mysql    |
| Server   | mariadb  |
| Username | user     |
| Password | password |
| Database | poc      |

## Load Test

At first you have to install `artillery` with

```bash
$ npm install -g artillery
```

so you can use the config in `./load-test-scenario/config.yml`. Unfortunately, I had no success to integrate `artillery` in the docker environment but I didn't spend so much time for investigating this issue because this is only a PoC.

To start `artillery` you can use the following command:

```bash
$ http_proxy= artillery run ./load-test-scenario/config.yml
```

Please notice, we must deactivate the http proxy setting. Documentation is tested with MacOS and I hope these settings will work similar on Windows.
