DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` mediumint,
  `name` varchar(255) default NULL,
  `street` varchar(255) default NULL,
  `postcode` varchar(10) default NULL,
  `city` varchar(255),
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `user` (`id`,`name`,`street`,`postcode`,`city`) VALUES (1000,"Alma","3388 Vitae Ave","66971","Tuglie"),(1001,"Yolanda","812-6655 Cras Av.","10424","Wibrin"),(1002,"Olga","P.O. Box 451, 4360 Mollis Street","42511","Paillaco"),(1003,"Ruth","328-2942 Rhoncus. Av.","10550","Huancayo"),(1004,"Susan","Ap #645-6210 Duis Ave","53985","Te Awamutu"),(1005,"Evangeline","662-9911 Donec Avenue","62606","Mont-Saint-Guibert"),(1006,"Shannon","Ap #266-2462 Enim. St.","87523","Heist-op-den-Berg"),(1007,"Paloma","4044 Odio. Rd.","15821","Bury St. Edmunds"),(1008,"Zoe","Ap #978-2216 Aliquet St.","90611","Kortrijk"),(1009,"Wynne","P.O. Box 800, 2475 Ligula. Ave","61179","Castelmezzano");
