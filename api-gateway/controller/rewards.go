package controller

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/schema"
	"gitlab.com/thesilk/microservice/api-gateway/models"
	"gitlab.com/thesilk/microservice/api-gateway/service"
)

func CalcReward(w http.ResponseWriter, r *http.Request) {
	var param models.RewardSearchParam
	if err := schema.NewDecoder().Decode(&param, r.URL.Query()); err != nil {
		log.Println(err)
	}

	host := getEnv("REWARDS_HOST", "127.0.0.1")
	port, err := strconv.ParseInt(getEnv("REWARDS_PORT", "1337"), 10, 32)
	if err != nil {
		log.Println("couldn't convert environment variable REWARDS_PORT to int32: %v\n", err)
	}

	protoClient := service.NewProtoRewards(host, int32(port))
	defer protoClient.Close()
	reward, err := protoClient.GetRewards(&param)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}

	respondWithJSON(w, http.StatusOK, reward)
}
