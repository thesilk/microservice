package controller

import (
	"log"
	"net/http"
	"strconv"

	"gitlab.com/thesilk/microservice/api-gateway/service"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	host := getEnv("SESSION_HOST", "127.0.0.1")
	port, err := strconv.ParseInt(getEnv("SESSION_PORT", "1338"), 10, 32)
	if err != nil {
		log.Println("couldn't convert environment variable SESSION_PORT to int32: %v\n", err)
	}

	protoClient := service.NewProtoUsers(host, int32(port))
	defer protoClient.Close()
	reward, err := protoClient.GetUsers()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}

	respondWithJSON(w, http.StatusOK, reward)
}
