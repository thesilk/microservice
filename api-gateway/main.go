package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/thesilk/microservice/api-gateway/controller"
)

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/rewards", controller.CalcReward).Methods("GET")
	r.HandleFunc("/users", controller.GetUsers).Methods("GET")

	log.Println("API Gateway listening on 0.0.0.0:2342...")
	http.ListenAndServe(":2342", r)

}
