package models

type RewardSearchParam struct {
	LastDays int32
	Username string
}
