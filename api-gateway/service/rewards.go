package service

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/thesilk/microservice/api-gateway/models"
	"gitlab.com/thesilk/microservice/protobuf"
	"google.golang.org/grpc"
)

type protoRewards struct {
	connection *grpc.ClientConn
}

func NewProtoRewards(ip string, port int32) protoRewards {
	var (
		grpcClient protoRewards
		err        error
	)

	addr := fmt.Sprintf("%s:%d", ip, port)

	opts := grpc.WithInsecure()
	grpcClient.connection, err = grpc.Dial(addr, opts)
	if err != nil {
		log.Fatal(err)
	}

	return grpcClient
}

func (g *protoRewards) GetRewards(param *models.RewardSearchParam) (int32, error) {

	client := protobuf.NewRewardServiceClient(g.connection)
	request := &protobuf.RewardSearchRequest{LastDays: param.LastDays, Username: param.Username}

	resp, err := client.Reward(context.Background(), request)
	if err != nil {
		return 0, err
	}
	return resp.Reward, nil
}

func (g *protoRewards) Close() {
	g.connection.Close()
}
