package service

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/thesilk/microservice/protobuf"
	"google.golang.org/grpc"
)

type protoUsers struct {
	connection *grpc.ClientConn
}

func NewProtoUsers(ip string, port int32) protoUsers {
	var (
		grpcClient protoUsers
		err        error
	)

	addr := fmt.Sprintf("%s:%d", ip, port)

	opts := grpc.WithInsecure()
	grpcClient.connection, err = grpc.Dial(addr, opts)
	if err != nil {
		log.Fatal(err)
	}

	return grpcClient
}

func (g *protoUsers) GetUsers() ([]*protobuf.User, error) {

	client := protobuf.NewUsersServiceClient(g.connection)
	request := &protobuf.UsersRequest{Name: ""}

	resp, err := client.User(context.Background(), request)
	if err != nil {
		return []*protobuf.User{}, err
	}
	return resp.Users, nil
}

func (g *protoUsers) Close() {
	g.connection.Close()
}
