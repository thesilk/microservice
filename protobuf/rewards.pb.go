// Code generated by protoc-gen-go. DO NOT EDIT.
// source: protobuf/rewards.proto

package protobuf

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type RewardSearchRequest struct {
	LastDays             int32    `protobuf:"varint,1,opt,name=lastDays,proto3" json:"lastDays,omitempty"`
	Username             string   `protobuf:"bytes,2,opt,name=username,proto3" json:"username,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RewardSearchRequest) Reset()         { *m = RewardSearchRequest{} }
func (m *RewardSearchRequest) String() string { return proto.CompactTextString(m) }
func (*RewardSearchRequest) ProtoMessage()    {}
func (*RewardSearchRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_1a20b6891df4c9bf, []int{0}
}

func (m *RewardSearchRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RewardSearchRequest.Unmarshal(m, b)
}
func (m *RewardSearchRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RewardSearchRequest.Marshal(b, m, deterministic)
}
func (m *RewardSearchRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RewardSearchRequest.Merge(m, src)
}
func (m *RewardSearchRequest) XXX_Size() int {
	return xxx_messageInfo_RewardSearchRequest.Size(m)
}
func (m *RewardSearchRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_RewardSearchRequest.DiscardUnknown(m)
}

var xxx_messageInfo_RewardSearchRequest proto.InternalMessageInfo

func (m *RewardSearchRequest) GetLastDays() int32 {
	if m != nil {
		return m.LastDays
	}
	return 0
}

func (m *RewardSearchRequest) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

type RewardSearchResponse struct {
	Reward               int32    `protobuf:"varint,1,opt,name=reward,proto3" json:"reward,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RewardSearchResponse) Reset()         { *m = RewardSearchResponse{} }
func (m *RewardSearchResponse) String() string { return proto.CompactTextString(m) }
func (*RewardSearchResponse) ProtoMessage()    {}
func (*RewardSearchResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_1a20b6891df4c9bf, []int{1}
}

func (m *RewardSearchResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RewardSearchResponse.Unmarshal(m, b)
}
func (m *RewardSearchResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RewardSearchResponse.Marshal(b, m, deterministic)
}
func (m *RewardSearchResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RewardSearchResponse.Merge(m, src)
}
func (m *RewardSearchResponse) XXX_Size() int {
	return xxx_messageInfo_RewardSearchResponse.Size(m)
}
func (m *RewardSearchResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_RewardSearchResponse.DiscardUnknown(m)
}

var xxx_messageInfo_RewardSearchResponse proto.InternalMessageInfo

func (m *RewardSearchResponse) GetReward() int32 {
	if m != nil {
		return m.Reward
	}
	return 0
}

func init() {
	proto.RegisterType((*RewardSearchRequest)(nil), "protobuf.rewardSearchRequest")
	proto.RegisterType((*RewardSearchResponse)(nil), "protobuf.rewardSearchResponse")
}

func init() {
	proto.RegisterFile("protobuf/rewards.proto", fileDescriptor_1a20b6891df4c9bf)
}

var fileDescriptor_1a20b6891df4c9bf = []byte{
	// 173 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0xe2, 0x12, 0x2b, 0x28, 0xca, 0x2f,
	0xc9, 0x4f, 0x2a, 0x4d, 0xd3, 0x2f, 0x4a, 0x2d, 0x4f, 0x2c, 0x4a, 0x29, 0xd6, 0x03, 0x0b, 0x08,
	0x71, 0xc0, 0xc4, 0x95, 0x7c, 0xb9, 0x84, 0x21, 0x52, 0xc1, 0xa9, 0x89, 0x45, 0xc9, 0x19, 0x41,
	0xa9, 0x85, 0xa5, 0xa9, 0xc5, 0x25, 0x42, 0x52, 0x5c, 0x1c, 0x39, 0x89, 0xc5, 0x25, 0x2e, 0x89,
	0x95, 0xc5, 0x12, 0x8c, 0x0a, 0x8c, 0x1a, 0xac, 0x41, 0x70, 0x3e, 0x48, 0xae, 0xb4, 0x38, 0xb5,
	0x28, 0x2f, 0x31, 0x37, 0x55, 0x82, 0x09, 0x28, 0xc7, 0x19, 0x04, 0xe7, 0x2b, 0xe9, 0x71, 0x89,
	0xa0, 0x1a, 0x57, 0x5c, 0x90, 0x9f, 0x57, 0x9c, 0x2a, 0x24, 0xc6, 0xc5, 0x06, 0x11, 0x87, 0x9a,
	0x06, 0xe5, 0x19, 0x45, 0x71, 0xf1, 0xc2, 0xd4, 0x17, 0x95, 0x65, 0x26, 0xa7, 0x0a, 0x79, 0x72,
	0xb1, 0x05, 0x81, 0x05, 0x84, 0x64, 0xf5, 0x60, 0x8e, 0xd4, 0xc3, 0xe2, 0x42, 0x29, 0x39, 0x5c,
	0xd2, 0x10, 0x1b, 0x95, 0x18, 0x9c, 0xb8, 0xa2, 0xe0, 0xde, 0x4c, 0x62, 0x03, 0xb3, 0x8c, 0x01,
	0x01, 0x00, 0x00, 0xff, 0xff, 0x5d, 0x0d, 0xc7, 0x08, 0x11, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RewardServiceClient is the client API for RewardService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RewardServiceClient interface {
	Reward(ctx context.Context, in *RewardSearchRequest, opts ...grpc.CallOption) (*RewardSearchResponse, error)
}

type rewardServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRewardServiceClient(cc grpc.ClientConnInterface) RewardServiceClient {
	return &rewardServiceClient{cc}
}

func (c *rewardServiceClient) Reward(ctx context.Context, in *RewardSearchRequest, opts ...grpc.CallOption) (*RewardSearchResponse, error) {
	out := new(RewardSearchResponse)
	err := c.cc.Invoke(ctx, "/protobuf.rewardService/Reward", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RewardServiceServer is the server API for RewardService service.
type RewardServiceServer interface {
	Reward(context.Context, *RewardSearchRequest) (*RewardSearchResponse, error)
}

// UnimplementedRewardServiceServer can be embedded to have forward compatible implementations.
type UnimplementedRewardServiceServer struct {
}

func (*UnimplementedRewardServiceServer) Reward(ctx context.Context, req *RewardSearchRequest) (*RewardSearchResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Reward not implemented")
}

func RegisterRewardServiceServer(s *grpc.Server, srv RewardServiceServer) {
	s.RegisterService(&_RewardService_serviceDesc, srv)
}

func _RewardService_Reward_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RewardSearchRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RewardServiceServer).Reward(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.rewardService/Reward",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RewardServiceServer).Reward(ctx, req.(*RewardSearchRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RewardService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "protobuf.rewardService",
	HandlerType: (*RewardServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Reward",
			Handler:    _RewardService_Reward_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "protobuf/rewards.proto",
}
