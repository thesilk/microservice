protobuf: clean
	@protoc --go_out=plugins=grpc:. ./protobuf/*.proto

run-api-gateway: protobuf
	go run api-gateway/main.go

run-rewards:
	go run rewards/main.go

run-session:
	go run session/main.go

clean:
	@rm -f ./protobuf/*.pb.go
