package server

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"

	"gitlab.com/thesilk/microservice/protobuf"
	model "gitlab.com/thesilk/microservice/session/models"
	"google.golang.org/grpc"
)

type Server struct {
	db          *sql.DB
	protoServer *grpc.Server
	err         error
}

func (s *Server) createDBConnection() {
	host := getEnv("DB_HOST", "127.0.0.1")
	port, err := strconv.ParseInt(getEnv("DB_PORT", "3306"), 10, 32)
	if err != nil {
		log.Printf("couldn't convert environment variable DB_PORT to int32: %v\n", err)
	}

	dbUrl := fmt.Sprintf("user:password@tcp(%s:%d)/poc", host, port)

	s.db, s.err = sql.Open("mysql", dbUrl)
	if s.err != nil {
		log.Println(s.err)
	}
}

func (s *Server) createGRPCServer() {
	s.protoServer = grpc.NewServer()
}

func (s *Server) Initialize() {
	s.createDBConnection()
	s.createGRPCServer()
}

func (s *Server) Run() {
	address := "0.0.0.0:1338"
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("Error %v", err)
	}
	fmt.Printf("Server is listening on %v ...\n", address)

	protobuf.RegisterUsersServiceServer(s.protoServer, s)

	s.protoServer.Serve(lis)
}

func (s *Server) User(ctx context.Context, request *protobuf.UsersRequest) (*protobuf.UsersResponse, error) {
	name := request.Name
	if name == "" {
		users, err := model.GetUsers(s.db)
		if err != nil {
			log.Println(err)
		}
		return &protobuf.UsersResponse{Users: users}, nil

	}
	puser := make([]*protobuf.User, 0)
	user, err := model.GetUser(s.db, name)
	if err != nil {
		log.Println(err)
	}
	puser = append(puser, user)
	return &protobuf.UsersResponse{Users: puser}, nil
}

func getEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = fallback
	}
	return value
}
