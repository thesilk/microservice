package model

import (
	"database/sql"

	"gitlab.com/thesilk/microservice/protobuf"
)

func GetUsers(db *sql.DB) ([]*protobuf.User, error) {
	users := make([]*protobuf.User, 0)

	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		return users, err
	}
	defer rows.Close()

	for rows.Next() {
		var user protobuf.User
		if err := rows.Scan(&user.Id, &user.Name, &user.Street, &user.Postcode, &user.City); err != nil {
			return []*protobuf.User{}, err
		}

		users = append(users, &user)
	}

	if err := rows.Err(); err != nil {
		return []*protobuf.User{}, err
	}
	return users, nil
}

func GetUser(db *sql.DB, name string) (*protobuf.User, error) {
	var user protobuf.User

	if err := db.QueryRow("SELECT * FROM user WHERE name = ?", name).Scan(&user.Id, &user.Name, &user.Street, &user.Postcode, &user.City); err != nil {
		return &user, err
	}

	return &user, nil
}
