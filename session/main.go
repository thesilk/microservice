package main

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/thesilk/microservice/session/server"
)

func main() {
	var server server.Server

	server.Initialize()
	server.Run()
}
