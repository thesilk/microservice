FROM golang:alpine AS builder
LABEL stage=builder

RUN apk add --no-cache gcc libc-dev
WORKDIR /workspace
COPY . .
WORKDIR /workspace/session
RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine AS final
WORKDIR /
COPY --from=builder /workspace/session/session .
CMD [ "./session" ]
