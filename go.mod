module gitlab.com/thesilk/microservice

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.3.3
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
	google.golang.org/grpc v1.29.1
)
