FROM golang:alpine AS builder
LABEL stage=builder

RUN apk add --no-cache gcc libc-dev
WORKDIR /workspace
COPY . .
WORKDIR /workspace/api-gateway
RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine AS final
WORKDIR /
COPY --from=builder /workspace/api-gateway/api-gateway .
CMD [ "./api-gateway" ]
